
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author sapon
 */
public class OX {

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'x';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean isFinish = false;
    static char winner = '-';
    static int count = 0;

    static void showWelcome() {
        System.out.println("Welcome to OX game");

    }

    static void showTable() {
        System.out.println("  1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");

        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;

            }

        }
        isFinish = true;
        winner = player;

    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;

            }
        }
        isFinish = true;
        winner = player;

    }

    static void checkX() {
        if (table[0][0] == player) {
            if (table[1][1] == player) {
                if (table[2][2] == player) {
                    isFinish = true;
                    winner = player;
                }

            }
        } else if (table[0][2] == player) {
            if (table[1][1] == player) {
                if (table[2][0] == player) {
                    isFinish = true;
                    winner = player;

                }
            }

        }

    }

    static void checkDraw() {
        if (count == 9 && winner != 'x' &&  != 'o') {
            isFinish = true;

        }

    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkX();

    }

    static void switchPlayer() {
        if (player == 'x') {
            player = 'o';
        } else {
            player = 'x';
        }

    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            System.out.println(winner + " Win!!");
        }

    }

    static void showBye() {
        System.out.println("Bye bye ....");

    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }

}
